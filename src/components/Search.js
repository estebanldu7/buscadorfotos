import React, {Component}  from 'react';

class Search extends Component{

    searchRef = React.createRef();

    getData = (e) => {
        e.preventDefault();

        const term = this.searchRef.current.value;

        this.props.searchedTerm(term);
    };

    render(){
        return(
            <form className={"row"} onSubmit={this.getData}>
                <div className={"form-group col-md-8"}>
                    <input ref={this.searchRef} className={"form-control form-control-lg"} type={"text"} placeholder={"Busca una imagen"}/>
                </div>
                <div className={"form-group col-md-4"}>
                    <input type={"submit"}  className={"btn btn-danger btn-lg btn-block"} value={"Buscar"} />
                </div>
            </form>
        )
    }
}

export default Search;