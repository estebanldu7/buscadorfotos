import React, {Component}  from 'react';

class Navigation extends Component{

    showBefore = () =>{
        const {page} = this.props;

        if(page > 1){
            return(
                <button onClick={this.props.previewPage} type={"button"} className={"btn btn-info mr-1"}>Anterior &larr;</button>
            )
        }
    };

    showNext = () => {
        const {page, totalPages} = this.props;

        if(totalPages === page){
           return null;
        }

        return(
            <button onClick={this.props.nextPage} type={"button"} className={"btn btn-info mr-1"}>Siguiente &rarr;</button>
        )
    };

    render(){
        return(
            <div className={"py-5"}>
                {this.showBefore()}
                {this.showNext()}
            </div>
        )
    }
}

export default Navigation;