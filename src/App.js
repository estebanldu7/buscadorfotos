import React, { Component } from 'react';
import Search from "./components/Search";
import Result from "./components/Result";
import './App.css';

class App extends Component {

  state = {
      term : '',
      images : [],
      page: '',
      charging : false,
      totalPages: ''
  };

  queryToApi = async() => {

    const term =  this.state.term;
    const page = this.state.page;
    const url = `https://pixabay.com/api/?key=11709269-a3bdcdca54ccf0e83351a5c05&q=${term}&per_page=30&page=${page}`;

    await fetch(url)
        .then(resp => {
            this.setState(() => {
                return {charging: true};
            });

            return resp.json()
        })
        .then(result =>  {

            const totalPagination = Math.ceil(result.totalHits / 30);

            setTimeout(() => {
                this.setState(() => {
                    return {
                        images: result.hits,
                        charging: false,
                        totalPages: totalPagination
                    };
                });
            }, 1500)
        })
   };

  seachedTerm = (term) => {
      this.setState(() => {
          return {term : term, page: 1};
      }, () => {
          this.queryToApi();
          this.scroll();
      });
  };

  previewPage = () => {
      let page = this.state.page;

      if(page > 1){
          page -= 1;

         this.setState(() => {
             return {page: page};
         }, () => {
             this.queryToApi();
             this.scroll();
         })
      }
  };

  nextPage = () => {
      let {page} = this.state;
      const {totalPages} = this.state;

      if(page === totalPages){
        return null;
      }

      page += 1;

      this.setState(() => {
          return {page: page};
      }, () => {
          this.queryToApi();
      })
  };

  scroll = () => {
      const scrollElement = document.querySelector('.jumbotron');
      scrollElement.scrollIntoView('smooth', 'start') //Hace scroll Automatico -  Inicia desde arriba de la pagina
  };

  render() {
    const charging = this.state.charging;
    let result;

    if(charging){
        result = <div className="spinner">
                    <div className="cube1"></div>
                    <div className="cube2"></div>
                 </div>
    }else{
        result = <Result
            images={this.state.images}
            previewPage={this.previewPage}
            nextPage={this.nextPage}
            page={this.state.page}
            totalPages={this.state.totalPages}
        />
    }

    return (
      <div className="app container">
        <div className={"jumbotron"}>
          <p className={"lead text-center"}>Buscador de Imagenes</p>
          <Search searchedTerm={this.seachedTerm} />
        </div>
        <div className={"row justify-content-center"}>
            {result}
        </div>
      </div>
    );
  }
}

export default App;
